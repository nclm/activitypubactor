<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>ActivityPub.Actor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#1da1f2">
        <meta property="og:title" content="Activitypub.actor" />
        <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css"/>
        <link rel="stylesheet" href="/general.css" type="text/css"/>
        <link rel="stylesheet" href="/index.css" type="text/css"/>
    </head>
    <body>
        <div class="content">
            <header>
                <h1>ActivityPub.Actor</h1>
                <p><i class="fa fa-heart" aria-hidden="true"></i></p>
                <h1>Twitter.com</h1>
            </header>
            <main>
                <p>WIP (beta) - <a href="https://framagit.org/DavidLibeau/activitypubactor" target="_blank">Open source</a> - <a href="https://mastodon.xyz/@David" target="_blank">Dev contact</a></p>
            </main>
        </div>
    </body>
</html>
